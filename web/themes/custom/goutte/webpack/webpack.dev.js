/**
 * Webpack development configuration
 */

import parseRuntime from './config/parse-runtime';

const webpack = require('webpack');
const Merge = require('webpack-merge');

const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = function (env) {
  const CommonConfig = require('./webpack.common.js')(env);
  const runtimeConfig = parseRuntime(env);

  const BROWSER_SYNC = !runtimeConfig.useDevServer && true;
  const EXTRACT_TEXT = !runtimeConfig.useHotModuleReplacement && (BROWSER_SYNC || true);

  let config = {
    devtool: 'cheap-source-map',
    mode: 'development',
    devServer: {
      contentBase: runtimeConfig.buildPath,
      publicPath: runtimeConfig.getRealPublicPath(),
      host: runtimeConfig.host,
      port: runtimeConfig.port,
      headers: {'Access-Control-Allow-Origin': '*'},
    },
    plugins: [
      new webpack.NamedModulesPlugin(),
      new FriendlyErrorsWebpackPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('development'),
        },
      }),
    ],
  };

  if (BROWSER_SYNC) {
    config.plugins.push(new BrowserSyncPlugin({
      proxy: runtimeConfig.proxy,
      files: [
        `${runtimeConfig.buildPath}/**/*.css`,
      ],
    }, {
      reload: false,
    }));
  }

  if (EXTRACT_TEXT) {
    config.plugins.push(new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: 'style.css',
    }));
  }

  return Merge(CommonConfig, config);
};
