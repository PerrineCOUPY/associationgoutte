import RuntimeConfig from './runtime.js';
import path from 'path';

/**
 * Generate runtime configuration from arguments and env variables
 */
let parseRuntime = (env) => {
    const runtimeConfig = new RuntimeConfig();
    let argv = require('yargs/yargs')(process.argv.slice(1)).argv;

    runtimeConfig.publicPath = '../../build/';
    runtimeConfig.buildPath = path.resolve(__dirname, '../../build');
    runtimeConfig.command = argv._[0];
    runtimeConfig.useDevServer = undefined !== process.argv.find(v => v.includes('webpack-dev-server'));
    runtimeConfig.useHotModuleReplacement = undefined !== process.argv.find(v => v.includes('--hot'));
    runtimeConfig.host = argv.hasOwnProperty('host') ? argv.host : 'localhost';
    runtimeConfig.port = argv.hasOwnProperty('port') ? argv.port : '8080';
    runtimeConfig.proxy = env.hasOwnProperty('proxy') ? env.proxy : 'localhost';
    runtimeConfig.devServerUrl = `http${runtimeConfig.devServerHttps ? 's' : ''}://${runtimeConfig.host}:${runtimeConfig.port}/`;

    return runtimeConfig;
};

export default parseRuntime;
