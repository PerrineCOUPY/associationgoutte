/**
 * Webpack base file.
 *
 * Use env parameters to configure :
 * - env.env : prod|dev
 * - env.proxy : proxy used by BrowserSync when used
 *
 * Example usage :
 * - `webpack` : build in dev env
 * - `webpack --env.env=prod` : build in production env
 * - `webpack --watch --env.proxy=127.0.0.1:8080` : watch for changes and run browsersync
 * - `webpack-dev-server` : start dev server
 *
 * Arguments can be passed through npm scripts with `--` before them :
 * - `npm start` : can run webpack-dev-server
 * - `npm start -- --host=10.0.0.2 --port=8080` : can start webpack dev server on host 10.0.0.2 and port 8080
 *
 */

module.exports = function(env) {
    env = env ||{};
    return require(`./webpack/webpack.${env.hasOwnProperty('env') ? env.env : 'dev'}.js`)(env);
};
