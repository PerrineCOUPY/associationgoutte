# Installation

## Fichiers
Remplacer THEME par le nom de votre thème partout (nom de fichiers et à l'intérieur)

## Installer
Lancer la commander `yarn install`

## Compiler
Lancer la commande yarn start avec le paramètre `--env.proxy=VOTRESITE.LOCAL`

Lancer la commande `yarn start`  => votre site local va se lancer sur le port 3000 et se mettra à jour à chaque fois que vous modifierez un fichier (attention vos caches doivent être désactivés sinon les modifications n'apparaîtront pas)

Webpack est configuré pour compiler l'ES6 et Sass,
il est également configuré pour ajouter les prefix de compatibilité navigateur.

Il y a un fichier variables.scss dans le dossier sass, qui permet de déclarer toutes les couleurs, polices, icones, breakpoint,...

Un fichier de mixins pour le responsive est présent, ils s'utilisent de la manière suivante :
```sass
@include below($md) {
 // pour exécuter du code en dessous du breakpoint défini(ici $md = desktop medium)
}

@include above($sm) {
 // pour exécuter du code au dessus du breakpoint défini (ici $sm pour small = mobile)
}
...
```

Une classe `limited-content` permet de définir un container avec une largeur maximale définie par la variable `$md-container`.

S'il est possible, utiliser une police pour les icônes SVG (utiliser icomoon et versionner le fichier de configuration )
