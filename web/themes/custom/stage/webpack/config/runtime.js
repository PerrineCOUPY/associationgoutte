/**
 * Handle runtime glboal configuration
 */
export default class RuntimeConfiguration {
    _constructor() {
        this.command = null;
        this.publicPath = null;
        this.environment = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';
        this.useDevServer = null;
        this.devServerUrl = null;
        this.devServerHttps = false;
        this.useHotModuleReplacement = null;
        this.host = null;
        this.port = null;
        this.proxy = null;
    }

    getRealPublicPath() {
        return this.useDevServer ? this.devServerUrl.replace(/\/$/,'') + this.publicPath : this.publicPath;
    }
}
