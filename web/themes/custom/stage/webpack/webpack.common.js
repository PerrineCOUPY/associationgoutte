import parseRuntime from './config/parse-runtime';

module.exports = function(env) {
    const runtimeConfig = parseRuntime(env);

    const webpack = require('webpack');

    const ManifestPlugin = require('./plugin/webpack-manifest-plugin');
    const MiniCssExtractPlugin = require('mini-css-extract-plugin');
    const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
    const Dotenv = require('dotenv-webpack');

    const BROWSER_SYNC = !runtimeConfig.useDevServer && true;
    const EXTRACT_TEXT = !runtimeConfig.useHotModuleReplacement && (BROWSER_SYNC || true);

    let cssConfig;
    let sassConfig;

    if (EXTRACT_TEXT) {
        cssConfig = [
            {
                loader: MiniCssExtractPlugin.loader,
            },
            'css-loader',
            'postcss-loader',
        ];

        sassConfig = [
            {
                loader: MiniCssExtractPlugin.loader,
            },
            'css-loader?sourceMap',
            'postcss-loader?sourceMap',
            'resolve-url-loader?sourceMap',
            'sass-loader?sourceMap',
        ];
    } else {
        cssConfig = [
            { loader: 'style-loader' },
            'css-loader?sourceMap',
            'postcss-loader?sourceMap',
        ];
        sassConfig = [
            { loader: 'style-loader' },
            'css-loader?sourceMap',
            'postcss-loader?sourceMap',
            'resolve-url-loader?sourceMap',
            'sass-loader?sourceMap',
        ];
    }

    return {
        entry: {
            index: './js/main.js',
        },
        output: {
            path: runtimeConfig.buildPath,
            publicPath: runtimeConfig.getRealPublicPath(),
            filename: '[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                    },
                },
                {
                    test: /\.css$/,
                    use: cssConfig,
                },
                {
                    test: /\.s[ac]ss$/,
                    use: sassConfig,
                },
                {
                    test: /\.(png|jpg|jpeg|gif|ico)$/,
                    loader: 'file-loader',
                    options: { name: 'image/[name].[ext]' },
                },
                {
                    test: /\.(woff|woff2|ttf|eot|otf)(\?[\s\S]+)?$/,
                    loader: 'file-loader',
                    options: { name: 'font/[name]-[hash].[ext]' },
                },
                {
                    test: [/\.svg$/],
                    loader: require.resolve('url-loader'),
                    options: {
                        limit: 10000,
                        name: 'static/media/[name].[hash:8].[ext]',
                    },
                },
            ],
        },
        plugins: [
            new SpriteLoaderPlugin(),
            new ManifestPlugin({
                publicPath: runtimeConfig.getRealPublicPath(),
                fileName: 'manifest.json',
                writeToFileEmit: true,
            }),
            new webpack.ProvidePlugin({
                '$': 'jquery',
                'jQuery': 'jquery',
            }),
            new Dotenv(),
            new webpack.ContextReplacementPlugin(/\.\/locale$/, 'empty-module', false, /js$/),
        ],
        optimization: {
            runtimeChunk: "single",
        },
        externals: {
          'jquery': 'jQuery',
          '$': 'jQuery'
        }
    };
};
