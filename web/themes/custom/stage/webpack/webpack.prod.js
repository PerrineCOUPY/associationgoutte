/**
 * Webpack production configuration
 */

const Merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const webpack = require('webpack');

module.exports = function (env) {

  const CommonConfig = require('./webpack.common.js')(env);

  return Merge(CommonConfig, {
    devtool: false,
    mode: 'production',
    output: {
      filename: '[name].js',
    },
    plugins: [
      new webpack.LoaderOptionsPlugin({
        minimize: true,
        debug: false
      }),
      new webpack.HashedModuleIdsPlugin(),
      new CompressionPlugin({
        asset: "[path].gz[query]",
        algorithm: "gzip",
        test: /\.js$|\.css$/,
        threshold: 10240,
        minRatio: 0.8,
      }),
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: 'style.css'
      }),
    ],
    optimization: {
      minimizer: [
        new TerserPlugin({
          parallel: true,
          terserOptions: {
            ecma: 6,
          },
        }),
      ]
    }
  });
};
