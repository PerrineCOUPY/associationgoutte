import "./polyfill";
import objectFitImages from 'object-fit-images';
import '../sass/style.scss';

objectFitImages(null, {watchMQ: true});
