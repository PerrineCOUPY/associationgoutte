import 'normalize.css';
import '@babel/polyfill';
import 'whatwg-fetch';
import 'element-closest/browser';
import 'intersection-observer';
import 'classlist-polyfill';

// forEach polyfill for IE11
(function () {
  if ( typeof NodeList.prototype.forEach === "function" ) return false;
  NodeList.prototype.forEach = Array.prototype.forEach;
})();

// remove polyfill for IE11
(function (arr) {
  arr.forEach(function (item) {
    if (item.hasOwnProperty('remove')) {
      return;
    }
    Object.defineProperty(item, 'remove', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        if (this.parentNode === null) {
          return;
        }
        this.parentNode.removeChild(this);
      }
    });
  });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);

// Add class if navigator IE11 or edge
if(/Edge\/\d./i.test(navigator.userAgent)) {
  document.querySelector('body').classList.add('edge');
}

if(/rv:11.0/i.test(navigator.userAgent)) {
  document.querySelector('body').classList.add('ie11');
}

if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
  document.querySelector('body').style.cursor = 'pointer';
}
