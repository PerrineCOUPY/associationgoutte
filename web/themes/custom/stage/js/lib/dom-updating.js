export const wrap = (el, wrapper) => {
  el.parentNode.insertBefore(wrapper, el);
  wrapper.appendChild(el);
};

export const prepend = (el, wrapper) => {
  wrapper.insertBefore(el, null);

  return el;
};

export const append = (el, wrapper) => {
  wrapper.appendChild(el);

  return el;
};

export const create = (tag, classes, innerText) => {
  const element = document.createElement(tag);
  element.classList.add(...classes);

  innerText && (element.innerText = innerText);

  return element;
};