/**
 * Local Storage access with expiration date.
 */
export default {
  /**
   * @param key Local Storage key
   * @param data Any data that can be JSON stringified
   * @param expirationDate Expiration date
   */
  set: (key, data, expirationDate = null) => {
    if (window.localStorage) {
      localStorage.setItem(key, JSON.stringify({
        data,
        expirationDate
      }));
    }
  },

  /**
   * Read local storage key. Will return null if not found or expired.
   *
   * @param key Local Storage key
   *
   * @return mixed|null
   */
  get: (key) => {
    if (!window.localStorage) {
      return null;
    }

    const storageData = JSON.parse(localStorage.getItem(key) || '{}');

    if (storageData.expirationDate && new Date(storageData.expirationDate) < new Date()) {
      localStorage.removeItem(key);
      return null;
    }

    return storageData.data || null;
  },

  /**
   * Delete a local storage key.
   *
   * @param key
   *
   * @returns {boolean}
   */
  remove: (key) => {
    if (!window.localStorage || !localStorage.getItem(key)) {
      return false;
    }

    localStorage.removeItem(key);

    return true;
  },

  /**
   * Clear local storage.
   */
  clear: () => {
    if (window.localStorage) {
      localStorage.clear();
    }
  },

  /**
   * Checks if local storage exists.
   * @param key
   * @returns {boolean}
   */
  exists: (key) => {
    if (!window.localStorage || !localStorage.getItem(key)) {
      return false;
    }

    const storageData = JSON.parse(localStorage.getItem(key) || '{}');
    if (storageData.expirationDate && new Date(storageData.expirationDate) < new Date()) {
      localStorage.removeItem(key);
      return false;
    }

    return true;
  }
};