# project installation

DRUSH=../vendor/bin/drush
PHPCS=docker run --rm -v `pwd`:/app willhallonline/drupal-phpcs

# QA Tools
show-php-errors:
	$(PHPCS) phpcs --error-severity=6 --warning-severity=6 --ignore=*/node_modules/* web/modules/custom/

fix-php:
	$(PHPCS) phpcbf --ignore=*/node_modules/* web/modules/custom/ web/themes/custom

config-import:
	cd web && $(DRUSH) cim -y

config-export:
	cd web && $(DRUSH) csex -y

clear:
	cd web && $(DRUSH) cr

updb:
	cd web && $(DRUSH) updb -y

sources:
	git pull
	composer install
	cd web/themes/custom/THEME && yarn run build

deploy: sources clear updb clear config-import clear
